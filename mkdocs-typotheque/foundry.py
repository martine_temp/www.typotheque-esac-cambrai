#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
from glob import glob
import xml.etree.ElementTree as ET
import freetype
from jinja2 import Template
import re
import os
import yaml
import shutil

def get_yaml(_file_, key, dic=False):
    result = {}
    with open(_file_) as f:
        docs = yaml.load_all(f, Loader=yaml.FullLoader)
        for doc in docs:
            for k, v in doc.items():
                if k == key:
                    if dic == True:
                        for sub_k  in doc[k]:
                            result.update(sub_k)
                        return result
                    else:
                        return doc[k]

_LICENSES_ = get_yaml('mkdocs.yml', 'licenses', True)
_TAGS_ = get_yaml('mkdocs.yml', 'tags')

MKDOCS_CONFIG = 'mkdocs.yaml'

FONTS_PATH = "docs/fonts/"
FONTS = glob(FONTS_PATH+"*/")
MDS_PATH = "docs/fonts/"
MDS = glob(MDS_PATH+"*.md")

EDITOR = 'nano'

TPL = '''---
title: {{fontname}}
font_family: {{fontname}}
designers:{% for designer in designers %}
 - [{{ designer }}, {{designerUrl}}]{% endfor %}
styles:{% for style in styles %}
 - {{ style }}{% endfor %}
paths:{% for path in paths %}
 - {{ path }}{% endfor %}
weight: 5
public: yes
---
'''

class bc:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def listFonts():
    result = {}
    i = 0
    print('')

    for folder in FONTS:
        name = folder.split('/')[-2] 
        if glob(folder+'*.md'):
            validate = bc.OKGREEN+'✔ '+bc.ENDC
            exist = True
        else:
            validate = ''
            exist = False 

        print('['+str(i)+'] '+validate+bc.BOLD+bc.OKBLUE+name+bc.ENDC + ' path:'+folder + '')
        result[i] = [folder, name, exist]
        i += 1

    return result 


def getFontsFiles(folder):
    resFonts = []
    for file in glob(folder+'/*'):
        if re.match('.*\.otf|.*\.ttf|.*\.woff', file):
            resFonts.append(file)

    return resFonts


def autoGenerate(Zip=False):

    for i, folder in enumerate(FONTS):
        nameZip = folder.split('/')[-2]

        if len(glob(folder+'*.zip')) < 1 and Zip == True:
            print('---- > ', len(getFontsFiles(folder)))
            print(folder)
            print('besoin d\'un zip')
            try:
                shutil.make_archive(folder, 'zip', folder)
            except: 
                print('zip failed')

            try:
                shutil.move(folder[:-1]+'.zip', folder)
            except:
                print('zip exist')

        if glob(folder+'*md'):
            print(folder, i, ' -> '+bc.BOLD+bc.OKBLUE+' ok'+bc.ENDC)
        else:
            print(folder, i, ' -> '+bc.BOLD+bc.OKGREEN+' generate !!!'+bc.ENDC)
            generateMd(Auto=True, data=i)

        getFontsFiles(folder)


def getStyles(files):
    styles = []
    urls = []

    for f in files:
        if re.match('.*\.otf|.*\.ttf|.*\.woff', f):
            

            face = freetype.Face(f)
            style = face.style_name.decode("utf-8")
            styles.append(style)
            f = f.split('/', 1)
            urls.append(f[1])

    return [styles, urls]


def generateMd(data=0, Auto=False):

    foldersFonts = listFonts()
    if Auto == False: 
        data = input("\n What font markdown do you want to generate ? [num] : ")
        folder = foldersFonts[int(data)] 
        print('\n '+bc.BOLD+'You want to generate -> '+bc.OKCYAN+folder[1]+bc.ENDC)
        if folder[2] == True:
            rep = input("\n This project already exists. Do you want to overwrite this file [y/n] ")
            if rep == "y":
                pass
            else:
                generateMd()
    else:
        folder = foldersFonts[int(data)] 

    if Auto == False: 

        project_url = input("\n Official project url : ")

        print('')

        i = 0
        for lic in _LICENSES_:
            print('     ['+str(i)+'] '+bc.OKBLUE+lic+bc.ENDC)
            i += 1

        licenses_list = list(_LICENSES_.items())
        license_i = input("\n Project license : [num] ")
        license = licenses_list[int(license_i)]
        
        print('')

        i = 0
        for tag in _TAGS_:
            print('     ['+str(i)+'] '+bc.OKBLUE+tag+bc.ENDC)
            i += 1
        
        tag_i = input("\n Project tags : [1,2,...] ")
        tag_i = tag_i.split(',')
        tags = [ _TAGS_[int(i)] for i in tag_i]

        print(tags)
    else:
        folder = foldersFonts[int(data)] 
        license = []
        license.append('')
        license.append('')
        project_url = tags = ''

    files = glob(r''+folder[0]+'*.*') 
    for f in files:
        if re.match('.*\.otf|.*\.ttf|.*\.woff', f):
            file_ = f
            continue
    face = freetype.Face(file_)
    fontname = face.family_name.decode("utf-8")
    font_files = getStyles(files)
    styles = font_files[0]
    files_paths = font_files[1]
    designers  = ['anonymous']
    ascender = face.ascender 
    descender = face.descender 

    template = Template(TPL)
    md_content = template.render(
            fontname=fontname,
            project_url=project_url,
            license=license[0],
            license_url=license[1],
            designers=designers,
            styles=styles,
            paths=files_paths,
            tags=tags,
            ascender=ascender,
            descender=descender 
            )
    md_path = folder[0]+'info-'+folder[1]+'.md'
    f = open(md_path, "w")
    f.write(md_content)
    f.close()
    print(md_content)

    if Auto == False: 
        rep = input('\n Do you want edit this file ? [y/n] ')

        if rep == 'y':
            os.system( EDITOR+' '+md_path)

        generateMd()

def deleteMd():
    foldersFonts = listFonts()
    data = input("\n What font do you want delete [num] : ")
    folder = foldersFonts[int(data)] 
    rep = input("\n"+bc.WARNING+"Do you really want to delete "+folder[1]+".md ?  [y/n] "+bc.ENDC)
    if rep == "y":
        print(folder[1])
        for file in glob(MDS_PATH+folder[1]+'/info*.md'):
            os.remove(file)
            print("\n"+folder[1]+'.md has been successfully deleted ! \n')
    deleteMd()

def editMd():
    foldersFonts = listFonts()
    data = input("\n What markdown do you want edit [num] : ")
    folder = foldersFonts[int(data)] 
    os.system( EDITOR+' '+MDS_PATH+folder[1]+'.md')
    editMd()

def mdList(folder, outputFile):
    types = ('*.otf', '*.ttf')
    fonts = []
    for files in types:
        fonts.extend(glob(folder+files))
    tpl_bonbon = '''---
title: {{fontname}}
fonts_bonbon:
{% for font in fonts %}  - {{ font }}
{% endfor %}---
    '''
    template = Template(tpl_bonbon)
    md_content = template.render(
            fontname='fonts-bonbon',
            fonts=fonts)
    f = open(outputFile, "w")
    f.write(md_content)
    f.close()
    print(md_content)

        
def Main():

    parser = argparse.ArgumentParser()
    parser.add_argument('-l','--list', help="List fonts.", action="store_true")
    parser.add_argument('-g', '--generate', help="Generate font markdown.", action="store_true")
    parser.add_argument('-d', '--delete', help="Delete font markdown.", action="store_true")
    parser.add_argument('-e', '--edit', help="Edit font markdown.", action="store_true")
    parser.add_argument('-t', '--test', help="Edit font markdown.", action="store_true")
    parser.add_argument('-a', '--auto', help="Generate automatic", action="store_true")
    parser.add_argument('-z', '--zip', help="zip fonts", action="store_true")
    parser.add_argument('-mdl', '--md-list', help="list to yaml files", action="store_true")

    args = parser.parse_args()


    if args.list:
        listFonts()

    if args.generate:
        generateMd()
        # generateMd(True, 2)
    
    if args.delete:
        deleteMd()
    
    if args.edit:
        editMd()
    
    if args.auto: 
        if args.zip:
            autoGenerate(Zip=True)
        autoGenerate()
    
    if args.md_list:
        mdList('fonts-bonbon/', 'fonts-bonbon.md')

    if args.test:
        print(_LICENSES_)

if __name__ == '__main__':
    Main()
