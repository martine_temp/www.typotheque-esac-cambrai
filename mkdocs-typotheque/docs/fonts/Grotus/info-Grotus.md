---
title: Grotus
font_family: Grotus
designers:
 - [Nicolas Buisset, ]
styles:
 - Regular
paths:
 - fonts/Grotus/Martine - Grotus.otf
weight: 5
public: yes
---
