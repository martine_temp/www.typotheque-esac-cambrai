---
title: Manusde
font_family: Manusde
designers:
 - [Rachel Delamer, ]
styles:
 - Regular
paths:
 - fonts/Manusde/Martine - Manusde.otf
weight: 2
public: yes
---
