---
title: Jenson Meplat
font_family: Jenson Meplat
designers:
 - [Gabin traverse, ]
styles:
 - Regular
paths:
 - fonts/JensonMeplat/Martine - GabinJensonMeplat.otf
weight: 3
public: yes
---
