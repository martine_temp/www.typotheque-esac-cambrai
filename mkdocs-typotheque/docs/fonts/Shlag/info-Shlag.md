---
title: Shlag
font_family: Shlag
designers:
 - [Helmi Bardaa, ]
styles:
 - Regular
paths:
 - fonts/Shlag/Martine - Shlag.otf
weight: 8
public: yes
---
