---
title: Didot Epine
font_family: Didot Epine
designers:
 - [Yun-Hsuan Ku, ]
styles:
 - Regular
paths:
 - fonts/Depine/Martine - Depine.otf
weight: 4
public: yes
---
