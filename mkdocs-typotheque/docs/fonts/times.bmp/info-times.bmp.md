---
title: times.bmp
font_family: times.bmp
designers:
 - [Teddy Mortier, ]
styles:
 - Regular
paths:
 - fonts/times.bmp/Martine - times.bmp.otf
weight: 4
public: yes
---
