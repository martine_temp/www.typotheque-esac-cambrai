---
title: La Polissaffiche
font_family: La Polissaffiche
designers:
 - [Sarah Rollin, ]
styles:
 - Regular
paths:
 - fonts/LaPollissaffiche/Martine - LaPolissaffiche.otf
weight: 7
public: yes
---
