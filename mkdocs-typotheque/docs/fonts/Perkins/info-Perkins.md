---
title: Perkins
font_family: Perkins
designers:
 - [Benjamin Vertu, ]
styles:
 - Regular
paths:
 - fonts/Perkins/Martine - Perkins.otf
weight: 3
public: yes
---
