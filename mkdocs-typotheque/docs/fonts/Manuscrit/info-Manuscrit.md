---
title: Manuscrit
font_family: Manuscrit
designers:
 - [Yun-Husan, ]
styles:
 - Regular
paths:
 - fonts/Manuscrit/Martine - Manuscrit.otf
weight: 4
public: yes
---
