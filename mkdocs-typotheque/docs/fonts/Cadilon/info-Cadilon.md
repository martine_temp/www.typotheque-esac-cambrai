---
title: Cadilon
font_family: Cadilon
designers:
 - [Seona Lim, ]
styles:
 - Regular
paths:
 - fonts/Cadilon/Martine - Cadilon.otf
weight: 5
public: yes
---
