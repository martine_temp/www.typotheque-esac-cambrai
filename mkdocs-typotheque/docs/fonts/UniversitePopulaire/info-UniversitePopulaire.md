---
title: UniversitePopulaire
font_family: UniversitePopulaire
designers:
 - [Lisa Janaszek, ]
styles:
 - Regular
paths:
 - fonts/UniversitePopulaire/Martine - UniversitePopulaire.otf
weight: 10
public: yes
---
