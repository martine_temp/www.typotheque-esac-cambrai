---
title: Cohn
font_family: Cohn
designers:
 - [Chloé Galasse, ]
styles:
 - Regular
paths:
 - fonts/Cohn_Sans/Martine - Cohn_Sans.otf
weight: 7
public: yes
---
