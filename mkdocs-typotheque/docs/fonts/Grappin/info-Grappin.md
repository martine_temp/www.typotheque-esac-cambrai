---
title: Grappin
font_family: Grappin
designers:
 - [Clément Duval, ]
styles:
 - Regular
paths:
 - fonts/Grappin/Martine - Grappin.otf
weight: 8
public: yes
---
