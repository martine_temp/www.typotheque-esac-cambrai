---
title: CD
font_family: CD
designers:
 - [Clément Duval, ]
styles:
 - Regular
paths:
 - fonts/CD/Martine - CD.otf
weight: 1
public: yes
---
