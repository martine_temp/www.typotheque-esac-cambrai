---
title: Cheltenhans
font_family: Cheltenhans
designers:
 - [Axel Fontenil, ]
styles:
 - Regular
paths:
 - fonts/Cheltenhans/Martine - Cheltenhans.otf
weight: 5
public: yes
---

