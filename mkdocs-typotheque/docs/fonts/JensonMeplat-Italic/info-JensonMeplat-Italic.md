---
title: Jenson Meplat
font_family: Jenson Meplat
designers:
 - [Gabin traverse, ]
styles:
 - Italic
paths:
 - fonts/JensonMeplat-Italic/Martine - GabinJensonMeplat-Italic.otf
weight: 3
public: yes
---
