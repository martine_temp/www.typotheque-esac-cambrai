---
title: Lowne
font_family: Lowne
designers:
 - [Laura Petit, ]
styles:
 - Regular
paths:
 - fonts/Lowne/Martine - Lowne.otf
weight: 5
public: yes
---
