---
title: Rabat
font_family: Rabat
designers:
 - [Chloé Philippon, ]
styles:
 - Regular
paths:
 - fonts/Rabat/Martine - Rabat.otf
weight: 4
public: yes
---
