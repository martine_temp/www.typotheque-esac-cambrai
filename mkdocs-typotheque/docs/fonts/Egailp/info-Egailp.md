---
title: Egailp
font_family: Egailp
designers:
 - [Célia Petitjean, ]
styles:
 - Regular
paths:
 - fonts/Egailp/Martine - Egailp.otf
weight: 2
public: yes
---
