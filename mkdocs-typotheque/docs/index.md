---
title: Bureau de Martine
---
Martine et les étudiant·e·s de l’École supérieure d’art et de communication de Cambrai créé·e·s des typographies.
Nos camarades dessinent des lettres, impriment des messages, examinent des caractères, jouent avec des balises et codent des mots.

Que de jolies choses se trouvent dans nos dossiers.

Entrez dans le bureau de Martine et découvrez de délicieuses typographies gratuites, originales et modifiables qui viendront nourrir vos créations.

* **Licence:** Site sous [GNU GPL](https://www.gnu.org/licenses/licenses.fr.html) et Typographies sous [SIL](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL).
* Le site et son contenu sont libres d'utilisations, de modifications, et de redistributions si les auteur·e·s sont crédité·e·s.
*  ㅤ
* **Contact:** [martine@esac-cambrai.net](martine@esac-cambrai.net)
* **D’autres Typothèque étudiantes:** [ESA 75](http://typotheque.le75.be/), [e162](http://e162.eu/fonderie), [la cambre](http://lacambretypo.be/fr/typefaces), [ESADTYPE](http://postdiplome.esad-amiens.fr/), [ecal](https://ecal-typefaces.ch/typefaces/).
*  ㅤ
* [Voir le jeu de caractère](Martine_caractere_set.txt)
*  ㅤ
* **Création commune entre:**
* **Accompagnateur et Développeur:** le collectif [Luuse](http://www.luuse.io/) (Antoine, Étienne, Romain).
* **Membres Typothèque de l'ÉSAC:** [Gabin](https://www.gabintraverse.art/), Jérémy, Ines, Grégoriane, Helmi, Laureline.
* **Participants au Workshop:** Paul, Eugénie, Valentin, Chici, Sarah.
* **Administration de l'ÉSAC:** Mickaël, Sandra, Marie.
* **Remerciements aux profeseurs:** David, Bruno, Gilles.
* Avec le soutient du Crous Nord Pas-de-Calais et de l’ÉSAC
